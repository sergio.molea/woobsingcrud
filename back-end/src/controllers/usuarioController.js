const Usuario = require('../models/usuario');

const usuarioController = {
 
	crearUsuario: (req , res)=>{
		let body = req.body;

		let usuario = new Usuario({
			nombres: body.nombres,
			apellidos: body.apellidos,
			direccion: body.direccion,			
			correo: body.correo,
			telefono: body.telefono
		});

		usuario.save(( err , usuarioGuardado)=>{
			if (err) {
				return res.status(500).json({
					ok:false,
					mensaje:"Ha ocurrido un error",
					errors:err
				});
			}
			if(!usuarioGuardado){
				return res.status(500).json({
					ok:false,
					mensaje:"No se ha podido crear el usuario",
				});	
			}

			return res.status(200).json({
				ok:true,
				mensaje:"Usuario creado exitosamente",
				usuario: usuarioGuardado
			});
		});
	},

	obtenerUsuario: (req , res)=>{
		let usuario_id = req.params.id;
		Usuario.find({"_id" : usuario_id},(err, usuarios)=>{
			if (err) {
                return res.status(500).json({
                        ok: false,
                        mensaje: 'Error obteniendo usuarios',
                        errors: err
                    })
            }
            return res.status(200).json({
            	ok:true,
            	usuario : usuarios[0]
            })
		});
	},

	obtenerUsuarios: (req , res)=>{

		Usuario.find({},(err, usuarios)=>{
			if (err) {
                return res.status(500).json({
                        ok: false,
                        mensaje: 'Error obteniendo usuarios',
                        errors: err
                    })
            }
            return res.status(200).json({
            	ok:true,
            	usuarios : usuarios
            })
		});
	},

	editarUsuario: (req , res)=>{
		let body =  req.body;

		Usuario.findById( body._id ,(err, usuario)=>{
			if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error obteniendo el usuario indicado',
                    errors: err
                });
            }

            usuario.nombres = body.nombres || usuario.nombres;
            usuario.apellidos = body.apellidos || usuario.apellidos;
            usuario.direccion = body.direccion || usuario.direccion;
            usuario.correo = body.correo || usuario.correo;
            usuario.telefono = body.telefono || usuario.telefono;

            usuario.save(( err , usuarioGuardado)=>{
				if (err) {
					return res.status(500).json({
						ok:false,
						mensaje:"Ha ocurrido un error",
						errors:err
					});
				}

				return res.status(200).json({
					ok:true,
					mensaje:"Usuario editado exitosamente",
					usuario: usuarioGuardado
				});
			});
		});
	},

	borrarUsuario: (req , res) => {
		let user_id = req.params.id;
		console.log(user_id)
        Usuario.findOneAndDelete({"_id" : user_id}, (err, usuarioEliminado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: `Error al borrar usuario`,
                    errors: err
                });
            }

            if (!usuarioEliminado) {
                return res.status(400).json({
                    ok: false,
                    message: `No existe un ususario con ese id`,
                });
            }

            return res.status(200).json({
                ok: true,
                mensaje:"Usuario eliminado exitosamente",
                usuario: usuarioEliminado
            });

        })
	}
}

module.exports = usuarioController;