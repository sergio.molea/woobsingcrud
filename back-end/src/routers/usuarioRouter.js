let usuarioController  = require('../controllers/usuarioController');
let express = require('express');
let router = express.Router();

router.get('/get/:id', usuarioController.obtenerUsuario);
router.get('/getAll', usuarioController.obtenerUsuarios);
router.post('/post', usuarioController.crearUsuario);
router.put('/put', usuarioController.editarUsuario);
router.delete('/delete/:id', usuarioController.borrarUsuario);

module.exports = router;