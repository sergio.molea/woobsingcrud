const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
	nombres: {type:String },
	apellidos: {type:String },
	direccion: {type:String },
	correo: {type:String },
	telefono: {type:String }
	// nombre: {type:String , required:[ true , 'El nombre es requerido']},
});

module.exports = mongoose.model('Usuario', usuarioSchema)
