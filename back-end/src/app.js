const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const server = require('http').Server(app);

//CONECTION DB
mongoose.connection.openUri('mongodb://localhost:27017/WOOBSING', { useNewUrlParser: true }, (err, resp) => {
    if (err) throw err;
    console.log(`Base de datos conectada`)
})

//IMPORTING ROUTES
const usuarioRouter = require('./routers/usuarioRouter');

//SETTINGS
app.set("port",process.env.PORT || 3000);
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});

//MIDDLEWARES
app.use(morgan('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: false }));

//ROUTES
app.use('/usuario',usuarioRouter);

//SERVER LISTENING
app.listen(app.get("port"), ()=>{
	console.log(`Escuchando en el puerto ${app.get("port")}`)
});
