import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//COMPONENTS
import { MenuDeOpcionesComponent } from './components/menu-de-opciones/menu-de-opciones.component';
import { CreacionComponent } from './components/usuario/creacion/creacion.component';
import { ListadoUsuarioComponent } from './components/usuario/listado-usuario/listado-usuario.component';


const routes: Routes = [
	{path:"usuario", children:[
		{path:"listado", component:ListadoUsuarioComponent},
		{path:"creacion", component:CreacionComponent},
		{path:"edicion/:id", component:CreacionComponent},
		{path:"menu", component:MenuDeOpcionesComponent},
		{path:"**", redirectTo:"menu", pathMatch:"full"}
	]},
	{path:"**", redirectTo:"usuario", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
