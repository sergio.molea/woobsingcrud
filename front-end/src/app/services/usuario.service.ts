import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
headers:HttpHeaders;
  constructor( private http : HttpClient) { 
  	this.headers =  new HttpHeaders({
  		"Content-Type": "application/json",
  	});
  }

  getUsers(){
  	return this.http.get(environment.api_url+"/usuario/getAll", {headers : this.headers});
  }

  getUser(id:any){
    return this.http.get(environment.api_url+"/usuario/get/"+id, {headers : this.headers});
  }

  createUser(usuario:any){
  	return this.http.post(environment.api_url+"/usuario/post",usuario,{headers : this.headers});
  }

  editUser(usuario:any){
  	return this.http.put(environment.api_url+"/usuario/put",usuario,{headers : this.headers});
  }

  deleteUser(id_user:any){
  	return this.http.delete(environment.api_url+"/usuario/delete/"+id_user,{headers : this.headers});
  }

}
