import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService} from "./../../../services/usuario.service"
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-creacion',
  templateUrl: './creacion.component.html',
  styleUrls: ['./creacion.component.scss']
})
export class CreacionComponent implements OnInit {
form:FormGroup;
user_id:any;
usuario:any;
edicion:boolean = false;
alert:any;
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private _usuarioService: UsuarioService) { 
      this.user_id = this.route.params["_value"].id;
      if (this.user_id && this.route.url["_value"][0].path == "edicion") {
        this.edicion = true;
        this.obtenerUsuario(this.user_id);
      }
  }

  ngOnInit(): void {
    if (!this.edicion) {
  	  this.form = this.getFormBuilder();
    }
  }

  getFormBuilder(usuario?:any){
  	return this.formBuilder.group({
  		nombres:[usuario ? usuario.nombres : "", Validators.required],
  		apellidos:[usuario ? usuario.apellidos : "", Validators.required],
  		direccion:[usuario ? usuario.direccion : "", Validators.required],
  		correo:[usuario ? usuario.correo : "", Validators.required],
  		telefono:[usuario ? usuario.telefono : "", Validators.required],
  	})
  }

  obtenerUsuario(id:string){
    this._usuarioService.getUser(id).subscribe((resp:any)=>{
      this.usuario = resp.usuario;
      this.form = this.getFormBuilder(this.usuario);
    },error=>{
      console.log(error);
    });
  }


  guardar(){
    if (this.edicion) {
      this.edicionUsuario();
    }else{
      this.crearUsuario();
    }
  }

  crearUsuario(){
    if (this.form.invalid) {
      this.alert = "Por favor ingrese todos los campos";
      setTimeout(()=>this.alert = null, 2000);
      return;
    }
    this._usuarioService.createUser(this.form.value).subscribe((resp:any) => { 
      this.router.navigate(["/usuario/menu"]);
    },error=>{
      console.log(error);
    })
  }

  edicionUsuario(){
    if (this.form.invalid) {
      this.alert = "Por favor ingrese todos los campos";
      setTimeout(()=>this.alert = null, 2000);
      return;
    }
    let data = this.form.value;
    data._id = this.usuario._id;
    this._usuarioService.editUser(data).subscribe((resp:any) => { 
      this.router.navigate(["/usuario/menu"]);
    },error=>{
      console.log(error);
    })
  }

}
