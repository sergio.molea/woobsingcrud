import { Component, OnInit } from '@angular/core';
import { UsuarioService} from "./../../../services/usuario.service"


@Component({
  selector: 'app-listado-usuario',
  templateUrl: './listado-usuario.component.html',
  styleUrls: ['./listado-usuario.component.scss']
})
export class ListadoUsuarioComponent implements OnInit {
usuarios:any;
  constructor( private _usuarioService: UsuarioService) { }

  ngOnInit(): void {
	this.listarUsuarios();
  }

  listarUsuarios(){
  	this._usuarioService.getUsers().subscribe((resp:any)=>{
  		this.usuarios = resp.usuarios;
  	},error=>{
  		console.log(error);
  	});
  }

  eliminarUsuario(id:string, index:any){
    this._usuarioService.deleteUser(id).subscribe((resp:any)=>{
      console.log(resp.mensaje);
      this.usuarios.splice(index,1);
    },error=>{
      console.log(error);
    });
  }

}
