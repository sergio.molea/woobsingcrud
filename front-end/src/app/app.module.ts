import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms"
//COMPONENTES
import { MenuDeOpcionesComponent } from './components/menu-de-opciones/menu-de-opciones.component';
import { CreacionComponent } from './components/usuario/creacion/creacion.component';
import { ListadoUsuarioComponent } from './components/usuario/listado-usuario/listado-usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuDeOpcionesComponent,
    CreacionComponent,
    ListadoUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
